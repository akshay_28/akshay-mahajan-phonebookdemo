package com.demo.phonebook.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mahajan on 05-10-2018.
 * Contact Details Pojo
 */

public class ContactModel implements Parcelable {

    private String contactId;
    private String contactName;
    private String contactNumber;
    private String contactOrganization;
    private String contactEmail;
    private String contactImage;

    public ContactModel(String contactId, String contactName, String contactNumber, String contactEmail,
                        String contactImage, String contactOrganization) {

        this.contactId = contactId;
        this.contactName = contactName;
        this.contactNumber = contactNumber;
        this.contactOrganization = contactOrganization;
        this.contactEmail = contactEmail;
        this.contactImage = contactImage;
    }

    protected ContactModel(Parcel in) {
        contactId = in.readString();
        contactName = in.readString();
        contactNumber = in.readString();
        contactOrganization = in.readString();
        contactEmail = in.readString();
        contactImage = in.readString();
    }

    public static final Creator<ContactModel> CREATOR = new Creator<ContactModel>() {
        @Override
        public ContactModel createFromParcel(Parcel in) {
            return new ContactModel(in);
        }

        @Override
        public ContactModel[] newArray(int size) {
            return new ContactModel[size];
        }
    };

    public String getContactId() {
        return contactId;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getContactOrganization() {
        return contactOrganization;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public String getContactImage() {
        return contactImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(contactId);
        parcel.writeString(contactName);
        parcel.writeString(contactNumber);
        parcel.writeString(contactOrganization);
        parcel.writeString(contactEmail);
        parcel.writeString(contactImage);
    }
}
