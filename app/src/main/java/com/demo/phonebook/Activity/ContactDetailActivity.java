package com.demo.phonebook.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.demo.phonebook.Model.ContactModel;
import com.demo.phonebook.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Mahajan on 05-10-2018.
 * Contact Detail Activity
 */

public class ContactDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
        setContentView(R.layout.contact_detail_layout);
        RelativeLayout contactNumberParentLayout, contactEmailParentLayout, contactCompanyParentLayout;
        TextView contactName, contactNumber, contactEmail, contactCompany;
        CircleImageView contactImage;

        contactImage = findViewById(R.id.contact_image);

        contactNumberParentLayout = findViewById(R.id.contact_number_parent);
        contactEmailParentLayout = findViewById(R.id.email_parent);
        contactCompanyParentLayout = findViewById(R.id.organization_parent);

        contactName = findViewById(R.id.contact_name);
        contactNumber = findViewById(R.id.contact_number_label);
        contactEmail = findViewById(R.id.email_label);
        contactCompany = findViewById(R.id.organization_label);


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        ContactModel contactModel = null;
        if (bundle != null) {
            contactModel = bundle.getParcelable("contactDetailModel");
        }

        if (contactModel != null) {

            String name = contactModel.getContactName();
            String number = contactModel.getContactNumber();
            String email = contactModel.getContactEmail();
            String company = contactModel.getContactOrganization();

            contactName.setText(name);
            if (TextUtils.isEmpty(number)) {
                contactNumberParentLayout.setVisibility(View.GONE);
            } else {
                contactNumber.setText(number);
            }

            if (TextUtils.isEmpty(email)) {
                contactEmailParentLayout.setVisibility(View.GONE);
            } else {
                contactEmail.setText(email);
            }
            if (TextUtils.isEmpty(company)) {
                contactCompanyParentLayout.setVisibility(View.GONE);
            } else {
                contactCompany.setText(company);
            }
            // Bitmap for imageview
            Bitmap image;
            if (!contactModel.getContactImage().equals("")
                    && contactModel.getContactImage() != null) {
                image = BitmapFactory.decodeFile(contactModel.getContactImage());
                if (image != null) {
                    contactImage.setImageBitmap(image);
                } else {
                    image = BitmapFactory.decodeResource(getResources(),
                            R.drawable.ic_action_contact_image);
                    contactImage.setImageBitmap(image);
                }
            } else {
                image = BitmapFactory.decodeResource(getResources(),
                        R.drawable.ic_action_contact_image);
                contactImage.setImageBitmap(image);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
