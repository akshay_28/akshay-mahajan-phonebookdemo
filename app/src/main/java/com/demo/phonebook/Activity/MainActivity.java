package com.demo.phonebook.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.demo.phonebook.Adapter.ContactListAdapter;
import com.demo.phonebook.Model.ContactModel;
import com.demo.phonebook.R;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    public static final int RequestPermissionCode = 1;
    RelativeLayout progressLayout;
    RecyclerView recyclerView;
    SearchView searchView;
    ContactListAdapter contactListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressLayout = findViewById(R.id.progress_layout);
        recyclerView = findViewById(R.id.recycler_view);
        searchView = findViewById(R.id.search_view);

        setToolbar();
        enableRuntimePermission(this, RequestPermissionCode);

    }

    public class BackgroundProcess extends AsyncTask<Void, Void, List<ContactModel>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<ContactModel> doInBackground(Void... voids){
            return getAllContacts();
        }

        @Override
        protected void onPostExecute(List<ContactModel> contactModelList) {
            super.onPostExecute(contactModelList);
            progressLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            initRecyclerView(contactModelList);
            serachViewConfig();
        }
    }

    private void enableRuntimePermission(Activity context, int RequestPermissionCode) {

        if (ActivityCompat.shouldShowRequestPermissionRationale(
                context,
                Manifest.permission.READ_CONTACTS)) {
            new BackgroundProcess().execute();
            Toast.makeText(context, "CONTACTS permission allows us to Access CONTACTS app", Toast.LENGTH_LONG).show();

        } else {
            ActivityCompat.requestPermissions(context, new String[]{
                    Manifest.permission.READ_CONTACTS}, RequestPermissionCode);
        }
    }

    public void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_home);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getResources().getString(R.string.app_name));
        }
    }

    private void initRecyclerView(List<ContactModel> appInfoList) {
        contactListAdapter = new ContactListAdapter(appInfoList, this);
        recyclerView.setLayoutManager(new WrapContentLinearLayoutManager(this));
        recyclerView.setAdapter(contactListAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }
    public class WrapContentLinearLayoutManager extends LinearLayoutManager {
        WrapContentLinearLayoutManager(Context context) {
            super(context);
        }
        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            try {
                super.onLayoutChildren(recycler, state);
            } catch (IndexOutOfBoundsException e) {
                Log.e("Error", "IndexOutOfBoundsException in RecyclerView happens");
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String per[], int[] permissionResult) {

        switch (requestCode) {

            case RequestPermissionCode:

                if (permissionResult.length > 0 && permissionResult[0] == PackageManager.PERMISSION_GRANTED) {
                    new BackgroundProcess().execute();
                } else {
                    Toast.makeText(MainActivity.this, "Permission Canceled, You cannot access CONTACTS.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public List<ContactModel> getAllContacts() {
        List<ContactModel> contactModelList = new ArrayList<>();
        Uri uri = ContactsContract.Contacts.CONTENT_URI; // Contact URI
        Cursor contactsCursor = getContentResolver().query(uri, null, null,
                null, ContactsContract.Contacts.DISPLAY_NAME + " ASC ");

        if (contactsCursor != null && contactsCursor.moveToFirst()) {
            do {
                long contactId = contactsCursor.getLong(contactsCursor.getColumnIndex("_ID"));
                Uri dataUri = ContactsContract.Data.CONTENT_URI; // URI to get data of contacts

                Cursor dataCursor = getContentResolver().query(dataUri, null,
                        ContactsContract.Data.CONTACT_ID + " = " + contactId, null, null);// Retrun data cusror represntative to


                // Strings to get all details
                String contactName = "";
                String mobilePhone = "";
                String otherPhone = "";
                String photoPath = "" + R.drawable.ic_action_home; // Photo path
                byte[] photoByte = null;// Byte to get photo
                String homeEmail = "";
                String workEmail = "";
                String companyName = "";


                String contactNumbers = "";
                String contactEmailAddresses = "";
                String contactOtherDetails = "";

                // Now start the cusrsor
                if (dataCursor != null && dataCursor.moveToFirst()) {
                    contactName = dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));// get the contact name

                    do {
                        if (dataCursor.getString(dataCursor.getColumnIndex("mimetype"))
                                .equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)) {

                            switch (dataCursor.getInt(dataCursor.getColumnIndex("data2"))) {

                                case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                    mobilePhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                    contactNumbers = mobilePhone;
                                    break;

                                case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                                    otherPhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                    if (TextUtils.isEmpty(mobilePhone)) {
                                        contactNumbers = otherPhone;
                                    }
                                    break;
                            }
                        }
                        if (dataCursor.getString(dataCursor.getColumnIndex("mimetype"))
                                .equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)) {

                            switch (dataCursor.getInt(dataCursor.getColumnIndex("data2"))) {
                                case ContactsContract.CommonDataKinds.Email.TYPE_HOME:

                                    homeEmail = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                    contactEmailAddresses = homeEmail;
                                    break;
                                case ContactsContract.CommonDataKinds.Email.TYPE_WORK:

                                    workEmail = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                    if (TextUtils.isEmpty(homeEmail)) {
                                        contactEmailAddresses = workEmail;
                                    }
                                    break;
                            }
                        }

                        if (dataCursor.getString(dataCursor.getColumnIndex("mimetype"))
                                .equals(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)) {
                            companyName = dataCursor.getString(dataCursor.getColumnIndex("data1"));// get company name
                            contactOtherDetails = companyName;
                        }

                        if (dataCursor.getString(dataCursor.getColumnIndex("mimetype"))
                                .equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)) {

                            photoByte = dataCursor.getBlob(dataCursor.getColumnIndex("data15")); // get photo in byte
                            if (photoByte != null) {

                                Bitmap bitmap = BitmapFactory.decodeByteArray(photoByte, 0, photoByte.length);
                                File cacheDirectory = getBaseContext().getCacheDir();
                                File tmp = new File(cacheDirectory.getPath() + "/phonebook" + contactId + ".png");
                                try {
                                    FileOutputStream fileOutputStream = new FileOutputStream(tmp);
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                                    fileOutputStream.flush();
                                    fileOutputStream.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                photoPath = tmp.getPath();
                            }
                        }

                    } while (dataCursor.moveToNext()); // Now move to next cursor

                    contactModelList.add(new ContactModel(Long.toString(contactId),
                            contactName, contactNumbers, contactEmailAddresses,
                            photoPath, contactOtherDetails));// Finally add items to array list
                }

            } while (contactsCursor.moveToNext());
        }
        return contactModelList;
    }

    public void serachViewConfig(){
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint("Search by name or contact");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                contactListAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                contactListAdapter.getFilter().filter(query);
                return false;
            }
        });
    }

}
