package com.demo.phonebook.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.demo.phonebook.Activity.ContactDetailActivity;
import com.demo.phonebook.Model.ContactModel;
import com.demo.phonebook.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Mahajan on 05-10-2018.
 * ContactList Adapter and Holder
 */

public class ContactListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private List<ContactModel> allContactModelList;
    private List<ContactModel> filteredContactList;
    private Context context;
    protected Filter filter;
    private String searchString = "";


    public ContactListAdapter(List<ContactModel> allContactModelList, Context context) {
        this.allContactModelList = allContactModelList;
        this.context = context;
        this.filter = new FilterContactList();
        this.filteredContactList = allContactModelList;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ContentView(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_row, parent, false));
    }



    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ContentView content = (ContentView) holder;
        final ContactModel contactModel = filteredContactList.get(position);

        content.contactNumber.setText(contactModel.getContactNumber());

        // Bitmap for imageview
        Bitmap image;
        if (!contactModel.getContactImage().equals("")
                && contactModel.getContactImage() != null) {
            image = BitmapFactory.decodeFile(contactModel.getContactImage());
            if (image != null) {
                content.circleImageView.setImageBitmap(image);
            } else {
                image = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.ic_action_contact_dark);
                content.circleImageView.setImageBitmap(image);
            }
        } else {
            image = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.ic_action_contact_dark);
            content.circleImageView.setImageBitmap(image);
        }
        content.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ContactDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("contactDetailModel", contactModel);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });

        int startPos = contactModel.getContactName().toLowerCase(Locale.US).indexOf(searchString.toLowerCase(Locale.US));
        int endPos = startPos + searchString.length();

        if (startPos != -1)
        {
            Spannable spannable = new SpannableString(contactModel.getContactName());
            ColorStateList accentColor = new ColorStateList(new int[][] { new int[] {}}, new int[] { Color.RED });
            TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, accentColor, null);

            spannable.setSpan(highlightSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            content.contactName.setText(spannable);
        }
        else
            content.contactName.setText(contactModel.getContactName());
    }

    @Override
    public int getItemCount() {
        return filteredContactList.size();
    }

    static class ContentView extends RecyclerView.ViewHolder {

        private TextView contactName, contactNumber;
        private CircleImageView circleImageView;

        ContentView(View itemView) {
            super(itemView);
            contactName = itemView.findViewById(R.id.contact_name);
            contactNumber = itemView.findViewById(R.id.contact_number);
            circleImageView = itemView.findViewById(R.id.contact_image);
            /*nameInitial = itemView.findViewById(R.id.name_initial);*/
        }
    }

    public Filter getFilter() {
        return filter;
    }

    private class FilterContactList extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            String charString = charSequence.toString();
            searchString = charString;
            if (charString.isEmpty()) {
                filteredContactList = allContactModelList;
            } else {
                List<ContactModel> filteredList = new ArrayList<>();
                for (ContactModel row : allContactModelList) {

                    if (row.getContactName().toLowerCase().contains(charString.toLowerCase()) || row.getContactNumber().toLowerCase().contains(charString.toLowerCase())) {
                        filteredList.add(row);
                    }
                }
                filteredContactList = filteredList;
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredContactList;
            filterResults.count = filteredContactList.size();
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if (filterResults.count > 0) {
                Log.println(Log.INFO, "filterResults", "FOUND");
                filteredContactList = ((ArrayList<ContactModel>) filterResults.values);
                notifyDataSetChanged();
            } else {
                Log.println(Log.INFO, "filterResults", "-");
            }
        }
    }

}
